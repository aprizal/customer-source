import "bootstrap/dist/css/bootstrap.min.css";
import Content from "../../components/Content/index.content";
const Source = (props) => {
  return (
    <div>
      <Content source={props} />
    </div>
  );
};

Source.getInitialProps = ({ query }) => {
  return {
    post: query,
  };
};

export default Source;
