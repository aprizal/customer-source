import Axios from "axios";

export async function getHistoryOrderByPage(token, id, perPage) {
    const response = await Axios.get(
        `${process.env.endPoint}/v3/orders/cust-vendor/${id}?page=1&per_page=${perPage}`,
        {
            headers: {
                Authorization: 'Bearer' + ' ' + process.env.apiBearer,
                token: token,
                'Acces-Control-Allow-Origin': true,
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
        }
        );
    return response.data.payload;
}
export async function getHistoryOrder(token, id) {
    const response = await Axios.get(
        `${process.env.endPoint}/v3/orders/customer-vendors/${id}`,
        {
            headers: {
                Authorization: 'Bearer' + ' ' + process.env.apiBearer,
                token: token,
                'Acces-Control-Allow-Origin': true,
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
        }
        );
    return response.data.payload;
}

export async function getOrderByStatus(token, id, perPage, status) {
    const response = await Axios.get(
        `${process.env.endPoint}/v3/orders/cust-vendor/${id}?page=1&per_page=${perPage}&status=${status}`,
        {
            headers: {
                Authorization: 'Bearer' + ' ' + process.env.apiBearer,
                token: token,
                'Acces-Control-Allow-Origin': true,
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
        }
    );
    return response.data.payload;
}

export async function getListVendor(token, id) {
    const response = await Axios.get(
        `${process.env.endPoint}/v3/customer/${id}/order`,
        {
            headers: {
                Authorization: 'Bearer' + ' ' + process.env.apiBearer,
                token: token,
                'Acces-Control-Allow-Origin': true,
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
        }
    );
    return response.data.payload;
}

// export async function getTracking(token, code) {
//     const response = await Axios.get(
//         `${process.env.endPoint}/v3/orders/detail/${code}`,
//         {
//             headers: {
//                 Authorization: 'Bearer' + ' ' + process.env.apiBearer,
//                 token: token,
//                 'Acces-Control-Allow-Origin': true,
//                 'Content-Type': 'application/json',
//                 Accept: 'application/json'
//             },
//         }
//     );
//     return response.data.payload;
// }

export async function getTracking(token, data) {
    const response = await Axios({
      method: 'Post',
      url: `${process.env.endPoint}/v3/order/tracking`,
      headers: {
        token: token,
        "Content-Type": "application/json",
        Authorization: 'Bearer' + ' ' + process.env.apiBearer,
        'Acces-Control-Allow-Origin': true,
      },
      data
    }
    );
    return response.data;
  }
  
