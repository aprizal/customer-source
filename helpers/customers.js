import Axios from "axios";

export async function getDataCustomerByPhone(phone, token) {
  const response = await Axios.get(
    // `${process.env.endPoint}/v3/customers/minishop/phone/+${phone.slice(1)}`,
    `${process.env.endPoint}/v3/customers/minishop/phone/${phone.includes('+') ? phone : '+' + phone}`,
    {
      headers: {
        token: token,
        "Content-Type": "application/json",
        Authorization: 'Bearer' + ' ' + process.env.apiBearer,
      },
    }
  );
  return response.data.payload.data;
}

export async function getDataOrderByCustomerId() {
  console.log("get data order by customer id");
}

export async function getCustomerById(id, token) {
  const response = await Axios.get(
    `${process.env.endPoint}/v3/customers/detail/${id}`,
    {
      headers: {
        token: token,
        "Content-Type": "application/json",
        Authorization: 'Bearer' + ' ' + process.env.apiBearer,
      },
    }
  );
  return response.data.payload;
}

export async function updateReview(token, data) {
  const response = await Axios({
    method: 'Post',
    url: `${process.env.endPoint}/v3/orders/rating`,
    headers: {
      token: token,
      "Content-Type": "application/json",
      Authorization: 'Bearer' + ' ' + process.env.apiBearer,
      'Acces-Control-Allow-Origin': true,
    },
    data
  }
  );
  return response.data;
}

export async function updateComment(token, data) {
  const response = await Axios({
    method: 'Put',
    url: `${process.env.endPoint}/v3/orders/comment`,
    headers: {
      token: token,
      "Content-Type": "application/json",
      Authorization: process.env.apiBearer,
      'Acces-Control-Allow-Origin': true,
    },
    data
  }
  );
  return response.data;
}
