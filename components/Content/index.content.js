import {
  useState,
  useEffect,
  useRef,
  useLayoutEffect
} from "react";
import {
  getDataCustomerByPhone,
  getCustomerById,
} from "../../helpers/customers";
import {
  getHistoryOrderByPage,
  getHistoryOrder,
  getListVendor
} from "../../helpers/orders";
import { Container } from "react-bootstrap";
import Spinner from 'react-bootstrap/Spinner'
import NavigationTitle from "./Navbar/index.navbar";
import TransactionList from "./TransactionList/index.transactionList";

const Content = (props) => {
  const ref = useRef(null);

  const [customerData, setCustomerData] = useState([]);
  const [customerIdData, setCustomerIdData] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [fullOrder, setFullOrder] = useState([]);
  const [token, setToken] = useState();
  const [perPage, setPerPage] = useState(10);
  const [maxOrders, setMaxOrders] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [stopLoad, setStopLoad] = useState(false);
  const [dataAllVendor, setDataAllVendor] = useState([]);

  async function customerDatas(phone, token) {
    const response = await getDataCustomerByPhone(phone, token);
    setCustomerData(response);
    dataCustomerId(response.id, token);
    dataListVendor(response.id, token)
    dataOrders(token, response.id, 10);
  }

  async function dataOrders(token, id, perPages) {
    const response = await getHistoryOrderByPage(token, id, perPages);
    const full_response = await getHistoryOrder(token, id);
    setFullOrder(full_response.data);
    setTotalPrice(response.total_purchase);
    setOrderData(response.data);
    setMaxOrders(response.total)
    setPerPage(perPages)
    setIsLoading(false)
  }

  async function dataOrdersFill(token, id, perPages, fill) {
    const response = await getHistoryOrderByPage(token, id, perPages);
    const fillRes = response.data.filter(user => user.vendor.id == fill)
    setIsLoading(false)
    setOrderData(fillRes)
    // setStopLoad(true)
  }

  async function dataCustomerId(id, token) {
    const response = await getCustomerById(id, token);
    setCustomerIdData(response);
  }

  async function dataListVendor(id, token) {
    const response = await getListVendor(token, id);
    setDataAllVendor(response)
    // setCustomerIdData(response);
  }

  useEffect(() => {
    const token = atob(props.source.post.token);
    const phone = atob(props.source.post.phone);
    customerDatas(phone, token);
    setToken(token);
  }, []);

  useLayoutEffect(() => {
    function handleScroll() {
      const node = Math.floor(window.innerHeight + window.scrollY) > ref.current.offsetHeight
      if (node == true && stopLoad == false) {
        var newPage = perPage + 10
        var maxPege = orderData.length
        var maxOrder = maxOrders.total_order
        setIsLoading(true)
        if (maxPege < maxOrder || maxOrder == undefined) {
          dataOrders(token, customerData.id, newPage);
        } else {
          setIsLoading(false)
          console.log('sudah habis')
        }
      }
    }
    document.addEventListener("scroll", handleScroll)
    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [customerData, perPage, stopLoad])

  const onFatching = (data) => {
    setStopLoad(data)
  }

  const onFillterData = (e) => {
    if (e.value != 'no filter') {
      dataOrdersFill(token, customerIdData.data.id, 100000, e.value)
      setIsLoading(true)
      setStopLoad(true)
    } else {
      dataOrders(token, customerIdData.data.id, 10)
      setIsLoading(true)
      setStopLoad(false)
    }
  }

  return (
    <div ref={ref} style={styleBody}>
      <NavigationTitle
        dataIdCustomer={customerIdData}
        dataOrder={orderData}
        fullOrder={fullOrder}
        totalPrice={totalPrice}
        totalOrder={maxOrders}
        countOrder={orderData.length}
        token={token}
        dataAllVendor={dataAllVendor}
        onFillOrder={onFillterData}
      />
      <Container style={{ marginTop: 270 }}>
        <TransactionList history={orderData} token={token} isFetch={onFatching} />
        {isLoading && stopLoad == false ?
          <div className="text-center">
            <Spinner animation="grow" variant="primary" size="sm" />
            <Spinner animation="grow" variant="secondary" size="sm" />
            <Spinner animation="grow" variant="success" size="sm" />
            <Spinner animation="grow" variant="danger" size="sm" />
            <Spinner animation="grow" variant="warning" size="sm" />
            <Spinner animation="grow" variant="info" size="sm" />
            <Spinner animation="grow" variant="dark" size="sm" />
          </div>
          : null}
      </Container>
    </div>
  );
};

const styleBody = {
  background: "#fafafa",
  maxWidth: "500px",
  margin: "0 auto",
};

export default Content;
