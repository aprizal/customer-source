import { useState } from "react";
import { Row, Col } from "react-bootstrap";
import { MdPhoneIphone, MdEmail, MdClose, MdCall, MdLocationOn, MdRecordVoiceOver, MdArrowDropDown, MdArrowDropUp } from 'react-icons/md';

const ProfilIndex = (props) => {
    const [showAll, setShowAll] = useState(false)
    return (
        <div style={styleContainer}>
            <div style={styleBodys}>
                <div style={styleHeadeMain}>
                    <div className="text-right" onClick={() => props.onClose()}>
                        <MdClose size={25} />
                    </div>
                    <div className="pt-4 pb-2">
                        <img src="https://storage.googleapis.com/stg-v3-bucket/1603358886992hanaboticon.png" style={styleHeadImg} />
                    </div>
                    <p className="pb-4">
                        {props.allData.customer_name}
                    </p>
                </div>
                <Row style={styleBodyCenter}>
                    {props.countData.map((data, i) =>
                        <Col className={i < 3 ? "pl-0 pr-0 mb-2" : "pl-0 pr-0"} key={i} xs={i < 3 ? 4 : 3}>
                            <div style={i < 3 ? styleCenterIcon : {display: !showAll ? 'none' : 'block'}}>
                                <p className="mb-1" style={{ fontSize: 11 }}>
                                    {`Order ${data.title}`}
                                </p>
                                <p className="font-weight-bold mb-1" style={{ color: '#6f42c1' }}>
                                    {data.value}
                                </p>
                            </div>
                        </Col>
                    )}
                    <Col xs={12} className="text-center" onClick={() => setShowAll(!showAll)}>
                        {!showAll ? <MdArrowDropDown/> : <MdArrowDropUp />}
                    </Col>
                </Row>
                <Row className="pt-3 pb-3 ml-5 mr-5 text-left">
                    <Col xs={6} className="mb-2">
                        <MdEmail size={25} style={{ color: '#6f42c1' }} />
                        <span className="ml-2">{props.allData.customer_email}</span>
                    </Col>
                    <Col xs={6} className="mb-2">
                        <MdPhoneIphone size={25} style={{ color: '#6f42c1' }} />
                        <span className="ml-2">{props.allData.customer_phone}</span>
                    </Col>
                </Row>
                <Row className="pt-1 mb-3 ml-5 mr-5 text-left" style={styleAutoScroll}>
                    {props.allData.customer_addresses.map((data, i) =>
                        <Col xs={12} className="mb-1 p-2" key={i}>
                            <div className="p-2" style={styleCardAddress}>
                                <div className="text-right"><span className="pr-1 font-weight-normal" style={styleCardType}>{data.address_name}</span></div>
                                <Row>
                                    <Col className="pr-1 text-center" xs={2} style={{ color: '#6f42c1' }}>
                                        <MdRecordVoiceOver size={20} />
                                    </Col>
                                    <Col className="pr-1 pl-1" xs={10}>
                                        <span className="pr-1 font-weight-normal" style={{ fontSize: 14 }}>{data.delivery_name}</span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="pr-1 text-center" xs={2} style={{ color: '#6f42c1' }}>
                                        <MdCall size={20} />
                                    </Col>
                                    <Col className="pr-1 pl-1" xs={10}>
                                        <span className="pr-1 font-weight-normal" style={{ fontSize: 14 }}>{data.delivery_phone}</span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="pr-1 text-center" xs={2} style={{ color: '#6f42c1' }}>
                                        <MdLocationOn size={20} />
                                    </Col>
                                    <Col className="pr-1 pl-1" xs={10}>
                                        <span className="pr-1 font-weight-normal" style={{ fontSize: 14 }}>{`${data.address}. ${data.address_detail}`}</span>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    )}
                </Row>
            </div>
        </div>
    );
};

const styleContainer = {
    position: 'fixed',
    top: '0px',
    left: '0px',
    height: '100%',
    width: '100%',
    zIndex: '1000',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
}

const styleBodys = {
    width: '500px',
    background: 'white',
    borderRadius: '8px',
    textAlign: 'center',
    position: 'relative',
}

const styleHeadeMain = {
    borderTopRightRadius: '8px',
    borderTopLeftRadius: '8px',
    background: '#a88dd9',
    color: 'white',
    padding: '14px',
    height: '250px'
}

const styleHeadImg = {
    border: '6px solid white',
    background: 'white',
    height: '80px',
    width: '80px',
    objectFit: 'cover',
    borderRadius: '100%'
}

const styleBodyCenter = {
    marginLeft: '30px',
    marginRight: '30px',
    marginTop: '-50px',
    background: 'rgb(250, 250, 250)',
    borderRadius: '0.25rem',
    boxShadow: 'rgb(224, 224, 224) 2px 2px 10px',
    overflow: 'hidden',
    border: 'none !important',
    height: '100% !important',
    padding: '6px'
}

const styleCenterIcon = {
    borderRight: '1px solid #6f42c1',
    borderLeft: '1px solid #6f42c1'
}

const styleCardAddress = {
    backgroundColor: 'rgb(255, 255, 255)',
    borderRadius: '8px',
    boxShadow: 'rgb(239, 239, 239) 2px 2px 4px',
    fontSize: '11px',
    border: '0.5px solid rgb(239, 239, 239)',
}

const styleAutoScroll = {
    overflow: 'auto scroll',
    maxHeight: '240px'
}

const styleCardType = {
    fontSize: '12px',
    padding: '2px',
    color: 'white',
    background: '#6f42c1',
    borderRadius: '5px'
}
export default ProfilIndex;
