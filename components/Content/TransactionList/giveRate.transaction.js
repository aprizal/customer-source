import { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { MdClose, MdCheck, MdWarning } from 'react-icons/md';
import { AiOutlineLike } from 'react-icons/ai';
import { updateReview, updateComment } from "../../../helpers/customers"
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'

const GiveRate = (props) => {
    const [rates, setRates] = useState('')
    const [ulasan, setUlasan] = useState('')
    const [successfully, setSuccessfully] = useState('none')

    async function giveRateOrders(token, data) {
        const response = await updateReview(token, data);
        setRates(response.payload.data.customer_rate)
    }

    async function giveComment(token, data) {
        const response = await updateComment(token, data);
        if (response.message == 'update data success') {
            setSuccessfully('yes')
        } else {
            setSuccessfully('no')
        }
    }

    const onRate = (e) => {
        setRates(e.rating)
        let data = {
            order_id: props.idOrder.id,
            customer_name: props.idOrder.vendor.slug,
            customer_phone: props.idOrder.customer.customer_phone,
            customer_rate: e.rating,
        }
        giveRateOrders(props.token, data)
    }

    const onSendUlasan = () => {
        let data = {
            order_id: props.idOrder.id,
            customer_name: props.idOrder.vendor.slug,
            customer_phone: props.idOrder.customer.customer_phone,
            customer_rate: rates,
            customer_comment: ulasan,
        }
        giveComment(props.token, data)
    }

    return (
        <div style={styleContainer}>
            {successfully == 'none' ?
                <div style={styleBodys}>
                    <div className="text-right p-1" onClick={() => props.onClose(null)}>
                        <MdClose size={25} />
                    </div>
                    <div className="p-1">
                        <AiOutlineLike size={50} style={{ color: '#6f42c1' }} />
                        <h4>Berikan Rating</h4>
                        <p>{`Bagaimana pengalaman belanja mu di ${props.idOrder.vendor.vendor_name} - ${props.idOrder.vendor.vendor_address}.`}</p>
                    </div>
                    <div className="text-center">
                        <Rater
                            total={5}
                            rating={props.idOrder.vendor_rating == null ? rates : props.idOrder.vendor_rating.rating}
                            onRate={(e) => onRate(e)}
                            style={{ fontSize: 50, lineHeight: "0.7", cursor: "pointer" }}
                        />
                        <div className="p-3">
                            <Form.Control as="textarea" rows="2" placeholder="Ketikan ulasan mu di sini" value={props.idOrder.vendor_rating == null ? ulasan : props.idOrder.vendor_rating.comment} onChange={(e) => setUlasan(e.target.value)} />
                        </div>
                    </div>
                    <div>
                        {props.idOrder.vendor_rating == null ?
                            <div className={ulasan.length < 10 || rates == ''  ? "btn-disble btn-sm btn-block small" : "btn-next btn-sm btn-block small"} onClick={() => ulasan.length < 10 || rates == ''  ? null : onSendUlasan()}>
                                Selesai
                            </div> : null
                        }
                    </div>
                </div> :
                <div style={styleBodys}>
                    <div className="text-right p-1" onClick={() => props.onClose(null)}>
                        <MdClose size={25} />
                    </div>
                    <div className="p-1">
                        {successfully == 'yes' ? <MdCheck size={50} style={{ color: '#6f42c1' }} /> : <MdWarning size={50} style={{ color: '#6f42c1' }} />}
                        {successfully == 'yes' ? <p>Terimakasih, rating yang anda berikan sangat berguna bagi kami.</p> : <p>Tolong Beri Rating Ulang</p>}
                    </div>
                    {successfully == 'yes' ?
                        <div className="btn-next btn-sm btn-block small" onClick={() => location.reload()}>
                            Tutup
                        </div>
                        :
                        <div className="btn-next btn-sm btn-block small" onClick={() => setSuccessfully('none')}>
                            Coba lagi
                        </div>
                    }
                </div>
            }
            <style jsx>{`
            .btn-next {
                color: #fff !important;
                background-color: rgb(111, 66, 193, 0.9) !important;
                border-color: rgb(111, 66, 193) !important;
                cursor: pointer;
            }
            .btn-next:hover{
                color: #fff !important;
                background-color: rgb(111, 66, 193) !important;
                border-color: rgb(111, 66, 193) !important;
            }
            .btn-disble{
                color: #fff !important;
                background-color: rgb(111, 66, 193, 0.5) !important;
                border-color: rgb(111, 66, 193) !important;
                cursor: pointer;
            }
            `}</style>
        </div>
    )
}

const styleContainer = {
    position: 'fixed',
    top: '0px',
    left: '0px',
    height: '100%',
    width: '100%',
    zIndex: '1000',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
}

const styleBodys = {
    width: '500px',
    background: 'white',
    borderRadius: '8px',
    textAlign: 'center',
    position: 'relative',
}

export default GiveRate