import { useState } from "react";
import TransactionDetail from "./detail.transaction";
import List from "./list.transaction";
import GiveRate from "./giveRate.transaction";


const TransactionList = (props) => {
  const [isDetail, setIsDetail] = useState(null);
  const [showGiveRate, setShowGiveRate] = useState(null);

  const getDetail = (index) => {
    setIsDetail(index);
    if(index == null){
      props.isFetch(false)
    }else{
      props.isFetch(true)
    }
  };

  const onShowGiveRate = (i) => {
    setShowGiveRate(i)
    setIsDetail(null)
    props.isFetch(true)
  }

  return (
    <div>
      {isDetail !== null ? (
        <TransactionDetail detail={props.history[isDetail]} index={isDetail} detailIndex={getDetail} token={props.token}/>
      ) : (
          <List giveRate={onShowGiveRate} order={props.history} detailIndex={getDetail}/>
        )}

      {showGiveRate !== null ? <GiveRate onClose={onShowGiveRate} token={props.token} idOrder={props.history[showGiveRate]} /> : null}
    </div>
  );
};

export default TransactionList;
