import { RupiahConvert } from "../../Extension/index.extension";

const List = (props) => {
  return props.order.map((data, i) => {
    let dates = new Date(data.invoice_detail.created_at);
    return (
      <div style={styleNewCard} onClick={() => props.detailIndex(i)}>
        <div style={styleImgCard}>
          <img style={styleImg} src={data.order_items[0].product.product_images.length == 0 ? '/logosss.png' : data.order_items[0].product.product_images[0].url} />
        </div>
        <div className="pr-0 container">
          <div className="small font-weight-bold" style={styleDiv}>
            <div>
              {data.order_number}
            </div>
            <div className="text-right">
              <span style={styleStatus}>
                {data.status}
              </span>
            </div>
          </div>
          <div className="small transaction-lists" style={styleDiv}>
            <div className="pl-1">
              <div>
                {data.order_items[0].product.product_name}
              </div>
              <div>
                {data.order_items.length} barang
            </div>
              <div className="font-weight-bold" style={{ color: 'rgb(255, 118, 38)' }}>
                {RupiahConvert((parseInt(data.total_price) + parseInt(data.delivery_price)))}
              </div>
            </div>
            <div>
              <div className="text-right">
                {dates.toLocaleString()}
              </div>
              <div className="text-right">
                {data.vendor.vendor_name}
              </div>
              {data.status == 'COMPLETED' ?
                <div className="text-right mt-3" style={{ cursor: "pointer" }} onClick={() => props.giveRate(i)}>
                  {data.vendor_rating == null ?
                    'Berikan Rating' : 'Rated'
                  }
                </div> : null
              }
            </div>
          </div>
        </div>
        <style jsx>{`
        @media only screen and (max-width: 400px){
          .transaction-lists{
            zoom: 80%;
            font-size: 100%
          }
        }
      `}</style>
      </div>
    );
  });
};

const styleNewCard = {
  height: '110px',
  marginBottom: '15px',
  padding: '8px',
  backgroundColor: 'white',
  borderRadius: '8px',
  boxShadow: 'rgb(138, 138, 138) 2px 2px 4px',
  display: 'flex',
}

const styleImgCard = {
  width: '90px',
  height: '90px',
  marginTop: 'auto',
  marginBottom: 'auto',
  borderRadius: '5px'
}

const styleImg = {
  width: '90px',
  height: ' 90px',
  borderRadius: '5px'
}

const styleDiv = {
  fontSize: '70%',
  marginBottom: '0.3rem',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'baseline',
}

const styleStatus = {
  background: 'rgb(111, 66, 193, 0.5)',
  padding: '4px',
  borderRadius: '5px',
  color: 'white'
}



export default List;
