import { useState } from "react";
import { Row, Col } from "react-bootstrap";
import { MdClose } from 'react-icons/md';

const ModalTracking = (props) => {

    return (
        <div style={styleContainer}>
            <div style={styleBodys}>
                {props.data == '' ? 'Loading ...' : props.data == 'err' ? 
                <div className="p-4">
                    <h4 className="text-center p-4">Maaf tidak ada tracking</h4>
                    <div className="text-center p-2 small" style={{borderRadius: 8, color: 'white', background: '#7646ff'}} onClick={() => props.onClose()}>Tutup</div> 
                </div>
                :
                    <div style={styleHeadeMain}>
                        <Row className="mr-1 ml-1">
                            <Col xs={10} className="font-weight-bold pr-0 pl-0 text-left">
                                <p className="mb-3">Status Pengiriman</p>
                            </Col>
                            <Col xs={2} className="text-right pr-0 pl-0">
                                <div onClick={() => props.onClose()}>
                                    <MdClose size={25} />
                                </div>
                            </Col>
                        </Row>
                        <hr style={{ marginTop: 14, marginBottom: 14 }} />
                        <Row className="mb-2 mp-2 mt-2">
                            <Col xs={12} className="text-left small">
                                <p className="mb-0">{props.pengirim}</p>
                                <p className="mb-0 font-weight-bold">{`No. Resi: ${props.codeTracking}`}</p>
                            </Col>
                        </Row>
                        <hr style={{ marginTop: 14, marginBottom: 14 }} />
                        {props.data.result.manifest === undefined ? null :
                            <div style={{ overflow: 'auto scroll', maxHeight: '200px' }}>
                                {props.data.result.manifest.slice(0).reverse().map((data, index) =>
                                    <Row className="mx-0 mh-3 text-secondary" style={{ textAlign: 'left', fontSize: 14, width: '100%', }} key={index}>
                                        <Col xs={2} className="d-flex flex-column align-items-center">
                                            <span style={index == 0 ? CircleStyle : CircleNoStyle}></span>
                                            <span style={index == 0 ? LineStyle : LineNoStyle}></span>
                                        </Col>
                                        <Col xs={10} className="border-bottom pb-2">
                                            <p style={index == 0 ? { color: '#7646ff', marginBottom: 0 } : { marginBottom: 0 }}>
                                                {data.manifest_description.toUpperCase()}
                                            </p>
                                            <p className="small font-weight-bold">
                                                {data.manifest_date} &nbsp;
                                            {data.manifest_time}
                                            </p>
                                        </Col>
                                    </Row>
                                )}

                            </div>
                        }
                    </div>
                }
            </div>
        </div>
    )
}

const styleContainer = {
    position: 'fixed',
    top: '0px',
    left: '0px',
    height: '100%',
    width: '100%',
    zIndex: '1000',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
}

const styleBodys = {
    width: '500px',
    background: 'white',
    borderRadius: '8px',
    position: 'relative',
}

const styleHeadeMain = {
    // borderTopRightRadius: '8px',
    // borderTopLeftRadius: '8px',
    borderRadius: 8,
    background: 'white',
    color: 'black',
    padding: '14px',
    height: 'auto',
}

const CircleStyle = {
    background: '#7646ff',
    color: '#7646ff',
    borderRadius: '100%',
    width: 9,
    height: 9
}

const CircleNoStyle = {
    background: 'gray',
    color: 'gray',
    borderRadius: '100%',
    width: 9,
    height: 9
}

const LineStyle = {
    background: '#7646ff',
    color: '#7646ff',
    height: "75%",
    fontSize: 4,
    width: 2
}

const LineNoStyle = {
    background: 'gray',
    color: 'gray',
    height: "75%",
    fontSize: 4,
    width: 2
}

export default ModalTracking;