import { Card, Row, Col, Media, Toast } from "react-bootstrap";
import { RupiahConvert } from "../../Extension/index.extension";
import { FaArrowLeft, FaCheck, FaCopy } from "react-icons/fa";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { useState } from "react";
import {
  getTracking
} from "../../../helpers/orders";
import ModalTracking from './tracking.transaction';
import Spinner from 'react-bootstrap/Spinner'

const TransactionDetail = (props) => {
  const [copied, setCopied] = useState(false);
  const [closed, setClosed] = useState(false);
  const [track, setTrack] = useState(false);
  const [datatrack, setDataTrack] = useState('');
  const [isLoadingView, setIsLoadingView] = useState(false);

  const onCopas = (params) => {
    params
      ? (setCopied(true), setClosed(false))
      : (setCopied(false), setClosed(true));
  };

  const onShowModalTrack = () => {
    console.log()
    const data = {
      courier: props.detail.shipping_method.split(' - ')[0].toLowerCase(),
      waybill: props.detail.tracking_code
    }

    // const data = {
    //   courier: 'sicepat',
    //   waybill: '000180323566'
    // }
    dataOrderTrac(props.token, data)
    setIsLoadingView(true)
  };

  const onHideModalTrack = () => {
    setTrack(false)
  };

  async function dataOrderTrac(token, code) {
    const responseNew = await getTracking(token, code);
    if (responseNew.error == undefined) {
      setDataTrack(responseNew.payload.data)
      setTimeout(() => {
        setIsLoadingView(false)
        setTrack(true);
      }, 300);
    } else {
      setDataTrack('err')
      setIsLoadingView(false)
      setTrack(true);
    }
  };



  return (
    <div>
      {isLoadingView ?
        <div style={styleContainer}>
          <div style={styleBodys}>
            <div className="text-center">
              <Spinner animation="border" size="lg" />
            </div>
          </div>
        </div>
        : null
      }
      {track ? <ModalTracking data={datatrack} onClose={onHideModalTrack} codeTracking={props.detail.tracking_code} pengirim={props.detail.shipping_method} /> : null}

      <Row className="mt-2 mb-3 pb-0">
        <Col xs={2} className="mt-2">
          <span
            style={{ color: "#6f42c1" }}
            onClick={() => props.detailIndex(null)}
          >
            <FaArrowLeft style={{ fontSize: 28 }} />
          </span>
        </Col>
        <Col xs={8} className="text-center">
          <div>
            <b>Nomor Order</b>
          </div>
          <CopyToClipboard
            text={props.detail.order_number}
            onCopy={() => onCopas(true)}
          >
            <small style={{ textDecoration: "underline", color: "#6f42c1" }}>
              {props.detail.order_number} <FaCopy color="#484848" />
            </small>
          </CopyToClipboard>
        </Col>
        <hr />
      </Row>
      <Card style={{ marginBottom: 30 }}>
        <div className="ml-4 mt-4 mb-2">
          <h5 className="mb-3">
            <b>Detail Transaksi</b>
          </h5>
          <hr />
          <Row>
            <Col xs={6}>
              <b>Dari Toko</b>
            </Col>
            <Col xs={6}>
              <b>Opsi Pengiriman</b>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              <Media>
                <img
                  width="20%"
                  height="10%"
                  alt="halosis"
                  src={
                    // "https://storage.googleapis.com/stg-v3-bucket/" +
                    process.env.imgPoint +
                    props.detail.vendor.profile_picture
                  }
                />
                <i style={{ marginTop: 8 }}>
                  {" "}
                  &nbsp;{props.detail.vendor.vendor_name}
                </i>
              </Media>
            </Col>
            <Col xs={6}>
              <div>
                {props.detail.shipping_method}
              </div>
              <div style={{ color: 'blue', cursor: 'pointer' }} onClick={() => onShowModalTrack()}>
                {props.detail.tracking_code == null ? null : props.detail.tracking_code}
              </div>
            </Col>
          </Row>
        </div>
        <div className="ml-4 mt-2">
          <Row>
            <Col xs={6}>
              <b>Status</b>
            </Col>
            <Col xs={6}>
              <b>Jumlah barang</b>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>{props.detail.status}</Col>
            <Col xs={6}>{props.detail.order_items.length}</Col>
          </Row>
        </div>
        <div className="ml-4 mt-2 mb-4">
          <Row>
            <Col xs={6}>
              <b>Nomor Penerima</b>
            </Col>
            <Col xs={6}>
              <b>Dikirim ke</b>
            </Col>
          </Row>
          <Row>
            <Col xs={6}>
              {`${props.detail.delivery_phone == null ? props.detail.customer.customer_phone : props.detail.delivery_phone} (
              ${props.detail.delivery_name == null ? props.detail.customer.customer_name : props.detail.delivery_name} )`}
            </Col>
            <Col xs={6}>
              {`${props.detail.address_detail}. ${props.detail.delivery_address} ${props.detail.address_note == null ? '' : ',' + props.detail.address_note}(${props.detail.address_name.split('*')[1] == undefined ? props.detail.address_name : props.detail.address_name.split('*')[1]})`}
            </Col>
          </Row>
        </div>
        <div className="ml-3">
          <h5>
            <b>Rincian Barang</b>
          </h5>
          <hr />
        </div>
        {props.detail.order_items.map((data, i) => {
          return (
            <Card.Body
              key={i}
              style={{ borderBottom: "1px solid #F0F0F0", height: 135 }}
            >
              <Row>
                <Col>
                  <Media>
                    <img
                      width="55%"
                      height="50%"
                      alt="halosis"
                      src={data.product.product_images.length == 0 ? '/logosss.png' : data.product.product_images[0].url}
                    />
                  </Media>
                </Col>
                <Col>
                  <b style={{ color: "#6f42c1" }}>
                    #{data.product.product_code}
                  </b>
                  {data.price == null ?
                    <div>
                      {data.product_variation_orders.map((datas, i) =>
                        <div key={i}>
                          <div>
                            {datas.quantity}x {data.product.product_name} - {datas.product_variation.product_variation_detail.variant_name}
                          </div>
                          <b>{RupiahConvert(datas.price)}</b>
                        </div>
                      )}
                    </div>
                    :
                    <div>
                      <div>
                        {data.quantity}x {data.product.product_name}
                      </div>
                      <b>{RupiahConvert(data.price)}</b>
                    </div>
                  }
                </Col>
              </Row>
            </Card.Body>
          );
        })}
        <br />
        <Row className="mr-2">
          <Col xs={12} className="text-right">
            <h6 className="font-weight-bold">
              Total :{" "}
              <span className="small font-weight-bold" style={{ color: "#6f42c1" }}>
                {RupiahConvert(parseInt(props.detail.total_price))}
              </span>
            </h6>
            {props.detail.order_promotion == null ? null :
              <div>
                <p className="small mb-1" style={{ marginTop: '-12px' }}>
                  (Total sudah termasuk diskon sebesar
                    <span className="pl-1" style={{ color: "#6f42c1" }}>
                    {props.detail.order_promotion.promotion_type != "PERCENTAGE" ? `Rp. ${props.detail.order_promotion.value}` : `${props.detail.order_promotion.value}%`}
                  </span>)
                  </p>
              </div>
            }
            <h6 className="font-weight-bold">
              Biaya pengiriman :{" "}
              <span className="small font-weight-bold" style={{ color: "#6f42c1" }}>
                {RupiahConvert(parseInt(props.detail.delivery_price))}
              </span>
            </h6>
            <h6 className="font-weight-bold">
              Total Belanja:{" "}
              <span className="small font-weight-bold" style={{ color: "#6f42c1" }}>
                {RupiahConvert(parseInt(props.detail.total_price) + parseInt(props.detail.delivery_price))}
              </span>
            </h6>
          </Col>
        </Row>
      </Card>
      <div
        aria-live="polite"
        aria-atomic="true"
        style={{
          position: "absolute",
          top: 315,
          maxWidth: "60%",
        }}
      >
        <center>
          <Toast delay={3000} show={copied} onClose={() => onCopas(false)}>
            <Toast.Header>
              <span className="">no.order berhasil disalin </span>
            </Toast.Header>
          </Toast>
        </center>
      </div>
    </div>
  );
};

const styleContainer = {
  position: 'fixed',
  top: '0px',
  left: '0px',
  height: '100%',
  width: '100%',
  zIndex: '1000',
  backgroundColor: 'rgba(0, 0, 0, 0.2)',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}

const styleBodys = {
  width: '500px',
  background: 'transparent',
  borderRadius: '8px',
  textAlign: 'center',
  position: 'relative',
}

export default TransactionDetail;
