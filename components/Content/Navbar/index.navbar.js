import { useState, useEffect } from "react";
import NewNavbar from "./newNavbar";
import { getPriceByStatus, getShippedCount } from "../../Extension/index.extension";

const NavigationTitle = (props) => {
  const [unPaidPrice, setUnPaidPrice] = useState(0);
  const [shippedCount, setShippedCount] = useState(0);

  // useEffect(() => {
  //   getPriceByStatus(props, "NEW", setUnPaidPrice);
  //   getShippedCount(props, setShippedCount)
  // }, [props.dataOrder]);

  return (
    <div className="sticky-head">
      <NewNavbar
        // paidPrice={props.totalPrice}
        // unPaidPrice={unPaidPrice}
        // onShipped={shippedCount}
        dataAllVendors={props.dataAllVendor}
        token={props.token}
        totalOrder={props.totalOrder}
        dataCustomer={props.dataIdCustomer}
        onFillOrder={props.onFillOrder}

      />
      <style jsx>{`
        .sticky-head{
          position: fixed;
          top: 0px;
          background: #fafafa;
          padding-bottom: 6px;
          max-width: 500px;
          z-index: 10
        }        
      `}</style>
    </div>
  );
};

export default NavigationTitle;
