import { useState, useEffect } from "react";
import { Row, Col } from "react-bootstrap";
import { AiOutlineUser } from "react-icons/ai";
import ProfilIndex from "../Profil/index.profil";
import { RupiahConvert } from "../../Extension/index.extension";
import {
  getOrderByStatus
} from "../../../helpers/orders";
import Spinner from 'react-bootstrap/Spinner'
import Select from "react-select";


const NewNavbar = (props) => {
  const [showProfil, setShowProfil] = useState(false);
  const [dataByStatus, setDataByStatus] = useState([]);
  const [isLoadingView, setIsLoadingView] = useState(false);

  const onCloseModal = () => {
    setShowProfil(false);
  };

  const options = [{
    id: 0,
    value: `no filter`,
    label: `No filter`,
  }];
  if (props.dataAllVendors.data != undefined) {
    var allVendor = props.dataAllVendors.data
    for (var i = 0; i < props.dataAllVendors.data.length; i++) {
      const datas = {
        id: allVendor[i].vendor_id,
        value: allVendor[i].vendor_id,
        label: `${allVendor[i].vendor_name} ( ${allVendor[i].total_order} )`,
      }
      options.push(datas)
    }
  }


  async function dataOrderStatus(token, id) {
    const responseNew = await getOrderByStatus(token, id, 5, "NEW");
    const responseNewRec = await getOrderByStatus(token, id, 5, "NEW RECEIPT");
    const responsePaid = await getOrderByStatus(token, id, 5, "PAID");
    const responsePrinted = await getOrderByStatus(token, id, 5, "PRINTED");
    const responseShipped = await getOrderByStatus(token, id, 5, "SHIPPED");
    const responseCompleted = await getOrderByStatus(token, id, 5, "COMPLETED");
    const responseCancel = await getOrderByStatus(token, id, 5, "CANCELLED");
    const responseExpired = await getOrderByStatus(token, id, 5, "EXPIRED");
    const responseReturn = await getOrderByStatus(token, id, 5, "RETURN");
    const responseReject = await getOrderByStatus(token, id, 5, "REJECT");
    const data = [
      { title: "all", value: props.totalOrder.total_order },
      { title: "new", value: responseNew.total.total_order },
      { title: "paid", value: responsePaid.total.total_order },
      { title: "newreceipt", value: responseNewRec.total.total_order },
      { title: "printed", value: responsePrinted.total.total_order },
      { title: "shipped", value: responseShipped.total.total_order },
      { title: "completed", value: responseCompleted.total.total_order },
      { title: "cancelled", value: responseCancel.total.total_order == undefined ? responseCancel.total : responseCancel.total.total_order },
      { title: "expired", value: responseExpired.total.total_order },
      { title: "return", value: responseReturn.total.total_order == undefined ? responseReturn.total : responseReturn.total.total_order },
      { title: "reject", value: responseReject.total.total_order == undefined ? responseReject.total : responseReject.total.total_order },
    ];
    setDataByStatus(data);
    setTimeout(() => {
      setIsLoadingView(false)
      setShowProfil(true)
    }, 300
    );
  }

  const onLoadingViewProfil = (token, id) => {
    setIsLoadingView(true)
    dataOrderStatus(token, id)
  }

  const onChangeTipe = (e) => {
    props.onFillOrder(e)
  }

  return (
    <div>
      <Row className="m-0 pb-4" style={styleBodyHead}>
        <Col className="my-auto" xs={10} style={{ fontSize: 20 }}>
          <img
            width={35}
            height={35}
            className="my-auto"
            src={"/logosss.png"}
            alt="halosis"
          />{" "}
          Histori Transaksi
        </Col>
        <Col
          className="text-rightpb-2 my-auto"
          xs={2}
          onClick={() => onLoadingViewProfil(props.token, props.dataCustomer.data.id)}
        >
          <AiOutlineUser size={30} />
        </Col>
        <Col className="mt-4 mb-4" xs={12}>
          <p className="mb-0" style={fontSmall}>
            Total Belanja Kamu
          </p>
          <Row>
            <Col xs={6}>
              <p className="font-weight-bold" style={{ fontSize: 20 }}>
                {props.totalOrder == '' ? <Spinner animation="border" size="sm" /> : RupiahConvert(props.totalOrder.total_purchased_price)}
              </p>
            </Col>
            <Col xs={6} className="text-right" style={{ paddingRight: '18px' }}>
              {/* {props.allData.dataOrder.length == 0 ? '' : props.allData.dataOrder[0].vendor.vendor_name} */}
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="p-2" style={styleBodyHead2}>
        <Col xs={6} style={{ borderRight: "1px solid #6f42c1", textAlign: 'center' }}>
          <p className="font-weight-bold mb-0" style={fontSmall}>
            Belum Dibayar
          </p>
          <p
            className="mb-1 font-weight-bold"
            style={{ fontSize: 20, color: "#9a7ad3" }}
          >
            {props.totalOrder == '' ? <Spinner animation="border" size="sm" /> : RupiahConvert(props.totalOrder.total_unpurchase)}
          </p>
        </Col>
        <Col xs={6} className="text-center">
          <p className="font-weight-bold mb-0" style={fontSmall}>
            Sedang Dikirim
          </p>
          <p
            className="mb-1 font-weight-bold"
            style={{ fontSize: 20, color: "#00FF00" }}
          >
            {props.totalOrder == '' ? <Spinner animation="border" size="sm" /> : props.totalOrder.total_shipped}
          </p>
        </Col>
        <small><i><span style={{ color: "red" }}>**</span> klik list untuk melihat detail transaksinya</i></small>
      </Row>

      <Row style={styleSearch}>
        <Col sm={6}>

        </Col>
        <Col sm={6}>
          <Select
            options={options}
            placeholder={options[0].label}
            onChange={(e) => onChangeTipe(e)}
            onClick={() => onClickAllVendor()}
          />
        </Col>
      </Row>

      {showProfil ? (
        <ProfilIndex onClose={onCloseModal} allData={props.dataCustomer.data} countData={dataByStatus} />
      ) : null}

      {isLoadingView ?
        <div style={styleContainer}>
          <div style={styleBodys}>
            <div className="text-center">
              <Spinner animation="border" size="lg" />
            </div>
          </div>
        </div>
        : null
      }
    </div>
  );
};

const styleBodyHead = {
  background: "#6f42c1",
  color: "white",
  paddingTop: "14px",
  borderTopRightRadius: "16px",
  borderTopLeftRadius: "16px",
  height: "200px",
};

const styleBodyHead2 = {
  marginLeft: "18px",
  marginRight: "18px",
  marginTop: "-64px",
  background: "rgb(250, 250, 250)",
  borderRadius: "5px",
  boxShadow: "rgb(224, 224, 224) 2px 2px 10px",
  overflow: "hidden",
  border: "none !important",
  height: "100% !important",
};

const fontSmall = {
  fontSize: 12,
};

const styleContainer = {
  position: 'fixed',
  top: '0px',
  left: '0px',
  height: '100%',
  width: '100%',
  zIndex: '1000',
  backgroundColor: 'rgba(0, 0, 0, 0.2)',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}

const styleBodys = {
  width: '500px',
  background: 'transparent',
  borderRadius: '8px',
  textAlign: 'center',
  position: 'relative',
}

const styleSearch = {
  padding: '4px 18px 0px 18px',
}

export default NewNavbar;
