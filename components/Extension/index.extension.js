export const Footer = () => {
  return (
    <React.Fragment>
      <footer>
        <a href="" target="_blank" rel="noopener noreferrer">
          Powered by Halosis
        </a>
      </footer>
    </React.Fragment>
  );
};

export const RupiahConvert = (value) => {
  var rupiah = "";
  var valuerev = value.toString().split("").reverse().join("");
  for (var i = 0; i < valuerev.length; i++)
    if (i % 3 == 0) rupiah += valuerev.substr(i, 3) + ".";
  return (
    "Rp. " +
    rupiah
      .split("", rupiah.length - 1)
      .reverse()
      .join("")
  );
};

export const getPriceByStatus = (props, status, state) => {
  let price = 0;
  props.dataOrder
    .filter((datas) => {
      return datas.status == status;
    })
    .map((orders) => {
      return (price += orders.total_price);
    });
  if (status == "PAID") {
    return state(price);
  } else if (status == "NEW") {
    return state(price);
  } else {
    return false;
  }
};

export const getShippedCount = (props, state) => {
  let count = props.dataOrder.filter((datas) => {
    return datas.status == "SHIPPED";
  }).length;
  state(count);
};
