import Head from "next/head";

const Header = () => {
  return (
    <Head>
      <title>Halosis - Extension</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
};

export default Header;
